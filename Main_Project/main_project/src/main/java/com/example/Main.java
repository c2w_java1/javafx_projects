package com.example;


import com.example.initialize.InitializeApp;

import javafx.application.Application;

public class Main {
    public static void main(String[] args) {
        System.out.println("Hello world!");
        Application.launch(InitializeApp.class,args);
    }
}