package com.example.controller;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;

import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.Slider;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.stage.Stage;
import java.util.HashMap;
import java.util.Map;

import com.example.firebaseCoding.DataService;

public class SignupController {

    private LoginController loginController;
    private DataService dataService;

    public SignupController(LoginController loginController) {
        
        this.loginController = loginController;
    }

    //initSignupScene
    public Scene createSignupScene(Stage primaryStage) {

        Label messageLabel = new Label("Welcome to VehicleHub.\n");
        messageLabel.setUnderline(true);
        messageLabel.setTextFill(Color.ORANGERED);
        messageLabel.setFont(new Font(30));
        messageLabel.setAlignment(Pos.CENTER);

        Label messageLabel2 = new Label("'Discover, Buy, and Sell Classic and Vintage Vehicles Join our community of enthusiasts and collectors. Whether you're looking to buy your dream vintage car or sell a cherished classic, you've come to the right place.\nSign up today to start exploring the finest collection of old vehicles.'");
        VBox vBoxMessage = new VBox(messageLabel,messageLabel2);
        //messegLabel.setLayoutX(110);
        Image logoImage = new Image("image/logo.jpg");
        ImageView logoImageView = new ImageView(logoImage);
        logoImageView.setFitWidth(150);
        logoImageView.setFitHeight(100);
        logoImageView.setMouseTransparent(false);
        //logoImageView.setViewport(Circle);
        

        HBox topBox = new HBox(30,logoImageView,vBoxMessage);
        topBox.setPrefSize(1920, 200);
        topBox.setLayoutX(120);
        topBox.setLayoutY(20);

        //-----------------------------------------/
        Button loginPageNavigator = new Button("Login");
        loginPageNavigator.setPrefSize(200, 50);
        loginPageNavigator.setFont(new Font(30));
        loginPageNavigator.setStyle("-fx-background-color:null");
        loginPageNavigator.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                loginController.showLoginScene();
            }
            
        });

        Button signupPageNavigator = new Button("Sign Up");
        signupPageNavigator.setTextFill(Color.TEAL);
        signupPageNavigator.setPrefSize(200, 50);
        signupPageNavigator.setFont(new Font(30));
        signupPageNavigator.setStyle("-fx-background-color:null;-fx-underline: false;-fx-underline-color: red; -fx-border-color:Turquoise; -fx-padding: 4px;");
        signupPageNavigator.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
               loginController.showSignupScene();
            }
            
        });

        HBox navigationButtonBox = new HBox(160,loginPageNavigator,signupPageNavigator);
        navigationButtonBox.setAlignment(Pos.CENTER_RIGHT);
        navigationButtonBox.setPrefSize(1700, 100);
       
       
    

        //----------------------------------------------//
        
        Label fNameLabel = new Label("First Name");
        fNameLabel.setStyle("-fx-text-fill:Black;");
        fNameLabel.setFont(new Font(20));
        TextField fNameTextField = new TextField();
        fNameTextField.setPromptText("Enter First Name");
        fNameTextField.setStyle("-fx-background-color:Black;-fx-text-fill:White;");
        //fNameTextField.setPrefSize(400, 50);

        VBox fieldBox1 = new VBox(10,fNameLabel,fNameTextField);
        fieldBox1.setMaxSize(500, 30);

        Label lNameLabel = new Label("Last Name");
        lNameLabel.setStyle("-fx-text-fill:Black;");
        lNameLabel.setFont(new Font(20));
        TextField lNameTextField = new TextField();
        lNameTextField.setPromptText("Enter Last Name");
        lNameTextField.setStyle("-fx-background-color:Black;-fx-text-fill:White;");
        //lNameTextField.setPrefSize(400, 50);

        VBox fieldBox2 = new VBox(10,lNameLabel,lNameTextField);
        fieldBox2.setMaxSize(500, 30);

        Label mNumberLabel = new Label("Mobile No.");
        mNumberLabel.setStyle("-fx-text-fill:Black;");
        mNumberLabel.setFont(new Font(20));
        TextField mNamTextField = new TextField();
        mNamTextField.setPromptText("Enter Mobile No.");
        mNamTextField.setStyle("-fx-background-color:Black;-fx-text-fill:White;");
       //mNamTextField.setPrefSize(400, 50);

        VBox fieldBox3 = new VBox(10,mNumberLabel,mNamTextField);
        fieldBox3.setMaxSize(500, 30);
               
        Label userLabel = new Label("Username");
        userLabel.setStyle("-fx-text-fill:Black;");
        userLabel.setFont(new Font(20));
        TextField userTextField = new TextField();
        userTextField.setPromptText("Enter Username");
        userTextField.setStyle("-fx-background-color:Black;-fx-text-fill:White;");
        //userTextField.setPrefSize(400, 50);

        VBox fieldBox4 = new VBox(10, userLabel,userTextField);
        fieldBox4.setMaxSize(500, 30);

        //userTextField.setStyle("-fx-background-color:");
        //userTextField.setStyle("-fx-text-fill:Red;");
                

        Label passLabel = new Label("Password");
        passLabel.setStyle("-fx-text-fill:Black;");
        passLabel.setFont(new Font(20));
        PasswordField passField = new PasswordField();
        passField.setPromptText("Enter Password");
        passField.setStyle("-fx-background-color:Black;-fx-text-fill:White;");

        VBox fieldBox5 = new VBox(10, passLabel,passField);
        fieldBox5.setMaxSize(500, 30);

        Button signupButton = new Button("Signup");
        signupButton.setFont(new Font(20));
        signupButton.setPrefSize(200, 40);
        signupButton.setStyle("-fx-text-fill:Black;");
        signupButton.setStyle("-fx-background-color:Turquoise;");
        signupButton.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                
                handleSignup(fNameTextField.getText(),lNameTextField.getText(),mNamTextField.getText(),userTextField.getText(),passField.getText());
                userTextField.clear();
                passField.clear();
            }
            
        });

        HBox buttonBox = new HBox(50, signupButton);
        buttonBox.setMaxSize(500, 40);
        buttonBox.setAlignment(Pos.CENTER);

        VBox all = new VBox(20, fieldBox1, fieldBox2, fieldBox3,fieldBox4,fieldBox5,buttonBox);
        all.setAlignment(Pos.CENTER_RIGHT);
        //vbox.setPrefSize(1700, 1000);
        //vbox.setLayoutY(60);  //not change
        //---------------------------------------------------//
        
        Image image = new Image("image/bg10.jpg");
        ImageView imageView = new ImageView(image);
        imageView.setFitWidth(1920);
        imageView.setFitHeight(1000);
        Pane iPane = new Pane();
        iPane.getChildren().add(imageView);

        VBox vBoxMain = new VBox(70,navigationButtonBox,all);
        vBoxMain.setPrefSize(1700, 900);
        vBoxMain.setAlignment(Pos.CENTER_RIGHT);
        vBoxMain.setLayoutY(50);
        
        Group gr = new Group(iPane,topBox,vBoxMain);

        return new Scene(gr, 1920, 1000);
    }

    private void handleSignup(String fName, String lName, String mNo, String username, String password) {

        DataService dataService;

        try {
            
            dataService = new DataService();
            Map<String, Object> data = new HashMap<>();
            data.put("password", password);

            data.put("username", username);
            dataService.addData("users", username,data);
            System.out.println("User registered successfully");
            dataService.showAlert("Success","User Created Successfully.");
            loginController.showLoginScene();

        } catch (Exception e) {
            e.printStackTrace();
            //dataService.showAlert("Error", "Failed to create user.");
        
        }

    }

    

}
