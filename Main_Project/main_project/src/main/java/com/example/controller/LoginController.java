package com.example.controller;


import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Border;
import javafx.scene.layout.BorderStroke;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Line;
import javafx.scene.text.Font;
import javafx.stage.Stage;
import java.util.concurrent.ExecutionException;

import com.example.dashbord.UserPage;
import com.example.firebaseCoding.DataService;


public class LoginController {
    

    private Stage primaryStage;
    private Scene loginScene;
    private Scene userScene;
    private DataService dataService;
    public static String key;
    private SignupController signupController;

    
    public LoginController(Stage primaryStage) {
        
        this.primaryStage = primaryStage;
        dataService = new DataService(); 
        initScenes(); 
    }

    private void initScenes() {
        initLoginScene();
    }

    public void initLoginScene() {

        Label messageLabel = new Label("Welcome to VehicleHub.\n");
        messageLabel.setUnderline(true);
        messageLabel.setTextFill(Color.ORANGERED);
        messageLabel.setFont(new Font(30));
        messageLabel.setAlignment(Pos.CENTER);

        Label messageLabel2 = new Label("'Discover, Buy, and Sell Classic and Vintage Vehicles Join our community of enthusiasts and collectors. Whether you're looking to buy your dream vintage car or sell a cherished classic, you've come to the right place.\nSign up today to start exploring the finest collection of old vehicles.'");
        VBox vBoxMessage = new VBox(messageLabel,messageLabel2);
        //messegLabel.setLayoutX(110);
        Image logoImage = new Image("image/logo.jpg");
        ImageView logoImageView = new ImageView(logoImage);
        logoImageView.setFitWidth(150);
        logoImageView.setFitHeight(100);
        logoImageView.setMouseTransparent(false);
        //logoImageView.setViewport(Circle);
        

        HBox topBox = new HBox(30,logoImageView,vBoxMessage);
        topBox.setPrefSize(1920, 200);
        topBox.setLayoutX(120);
        topBox.setLayoutY(20);

        //---------------------------------------------//

        Button loginPageNavigator = new Button("Login");
        loginPageNavigator.setTextFill(Color.TEAL);
        loginPageNavigator.setPrefSize(200, 30);
        loginPageNavigator.setFont(new Font(30));
        loginPageNavigator.setStyle("-fx-background-color:null;-fx-underline: false;-fx-underline-color: red; -fx-border-color:Turquoise; -fx-padding: 4px;");
        //loginPageNavigator.setUnderline(true);
        //loginPageNavigator.setStyle("-fx-stroke: red; -fx-stroke-width: 1px;");
               
        loginPageNavigator.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {

                showLoginScene();
            }
            
        });

        Button signupPageNavigator = new Button("Sign Up");
        signupPageNavigator.setPrefSize(200, 50);
        signupPageNavigator.setFont(new Font(30));
        signupPageNavigator.setStyle("-fx-background-color:null");
        signupPageNavigator.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
               showSignupScene();
            }
            
        });

        HBox navigationButtonBox = new HBox(160,loginPageNavigator,signupPageNavigator);
        navigationButtonBox.setAlignment(Pos.CENTER_RIGHT);
        navigationButtonBox.setPrefSize(1700, 100);
        //navigationButtonBox.setLayoutX(10);
        //navigationButtonBox.setLayoutY(50);
        //navigationButtonBox.setStyle("-fx-background-color:yellow");

        //-----------------------------------------------------------------------//
        Label userLabel = new Label("Username");
        userLabel.setStyle("-fx-text-fill:Black;");
        userLabel.setFont(new Font(20));
        TextField userTextField = new TextField();
        userTextField.setPromptText("Enter Username");
        userTextField.setStyle("-fx-background-color:Black;-fx-text-fill:White;");
                

        Label passLabel = new Label("Password");
        passLabel.setStyle("-fx-text-fill:Black;");
        passLabel.setFont(new Font(20));
        PasswordField passField = new PasswordField();
        passField.setPromptText("Enter Password");
        passField.setStyle("-fx-background-color:Black;-fx-text-fill:White;");


        Button loginButton = new Button("Login");
        loginButton.setFont(new Font(20));
        loginButton.setPrefSize(200, 40);
        loginButton.setStyle("-fx-text-fill:Black;");
        loginButton.setStyle("-fx-background-color:Turquoise;");
        loginButton.setAlignment(Pos.CENTER);
        loginButton.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                
                
                handleLogin(userTextField.getText(),passField.getText());
                userTextField.clear();
                passField.clear();
            }
            
        });

        
        VBox fieldBox1 = new VBox(10, userLabel,userTextField);
        fieldBox1.setMaxSize(500, 30);
        VBox fieldBox2 = new VBox(10, passLabel,passField);

        fieldBox2.setMaxSize(500, 40);
        HBox buttonBox = new HBox(loginButton);

        buttonBox.setMaxSize(500, 30);
        buttonBox.setAlignment(Pos.CENTER);

       //userTextField.setStyle("-fx-set-pref-width:250,-fx-set-pref-hight:50");
       //passField.setStyle("-fx-set-pref-width:250,-fx-set-pref-hight:50");

        VBox all = new VBox(20,fieldBox1, fieldBox2, buttonBox);
        all.setAlignment(Pos.CENTER_RIGHT);
   
      
        Image image = new Image("image/bg10.jpg");
        ImageView imageView = new ImageView(image);
        imageView.setFitWidth(1920);
        imageView.setFitHeight(1000);
    
        Pane iPane = new Pane();
        iPane.getChildren().addAll(imageView);
      
        //vbox.setStyle("-fx-background-image:url('resources\\image\\bg7.jpg')");
        VBox vBoxMain = new VBox(100,navigationButtonBox,all);
        vBoxMain.setPrefSize(1700, 900);
        vBoxMain.setAlignment(Pos.TOP_CENTER);

        vBoxMain.setLayoutY(150);
        //navigationButtonBox.setAlignment(Pos.TOP_CENTER);
        
        
        
        Group gr = new Group(iPane,topBox,vBoxMain);
        

        loginScene = new Scene(gr,1920,1000);
        loginScene.setFill(Color.BLACK);        

    }
    
    private void initUserScene() {

        UserPage userPage = new UserPage(dataService);
        userScene = new Scene(userPage.createUserScene(this::handleLogout),1000, 800); 
    }

    public Scene getLoginScene() {
        return loginScene;
    }

    public void showLoginScene() {

        primaryStage.setScene(loginScene);
        primaryStage.setTitle("Login Page");
        primaryStage.show();
    }

    private void handleLogin(String username, String password) {
        
        
        try {
            if(dataService.authenticateUser(username,password)){ 
            
                key = username;
                initUserScene();
                primaryStage.setScene(userScene);
                primaryStage.setTitle("UserDashboard");
            } else{

                System.out.println("Invalid credentials");
                dataService.showAlert("Invalid Login","Invalid Credential.........!");
                key = null;
            }
        } catch (ExecutionException | InterruptedException ex) {
            

            ex.printStackTrace();
        }
            
    }

    public void showSignupScene() {

        SignupController signupController = new SignupController(this);
        Scene signupScene = signupController.createSignupScene(primaryStage);
        

        primaryStage.setScene(signupScene);
        primaryStage.setTitle("Signup Page");
        primaryStage.show();
    }

    private void handleLogout() {
        primaryStage.setScene(loginScene);
        primaryStage.setTitle("Login Page");
    }


}
