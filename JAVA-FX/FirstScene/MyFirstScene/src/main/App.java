package main;


import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.Stage;


public class App extends Application {
    Text tital =  null;
    Text java = null;
    Text cpp = null;
    Text python = null;

    public static void main(String[] args) throws Exception {
        System.out.println("Hello, World!");
        launch(args);
    }
    
    @Override
    public void start(Stage prStage){

        tital = new Text(100,100, "Programming languages.");
        tital.setFont(new Font(100));
  /*       
        Text lib = new Text("Lib");
        Text src = new Text("src");
        Text bin = new Text("bin");

        VBox vb = new VBox(20,bin,lib ,src);
        vb.setLayoutX(500);
        vb.setLayoutY(500);

        HBox hb = new HBox(vb);
*/
        Group gr = new Group(tital,cpp,java,python);

        Scene sc = new Scene(gr);
        sc.setFill(Color.BLACK);
    
        prStage.setScene(sc);

        prStage.setTitle("MyFirstProjectStage");
        prStage.getIcons().add(new Image("assets/image/shambhuraje.jpg"));
        prStage.setHeight(1000);
        prStage.setWidth(1000);
        prStage.setResizable(false);
        
        prStage.show();
    }
}
