package javaFiles.buttons;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.BorderStroke;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.stage.Stage;

public class Assignment2Buttons extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception {
        
       
      /* 
      *
      *1) 
        Button hSX = new Button("Hello Super - x");
       // hSX.setAlignment(Pos.CENTER);
        hSX.setLayoutX(800);
        hSX.setLayoutY(400);

        Label lb = new Label();
        lb.setFont(new Font(30));

        hSX.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                
                lb.setText("Core2web");
                System.out.println("Core2web");

            }
            
        });
        */
        Label c2w = new Label("Core2web.in");
        c2w.setFont(new Font(50));
        c2w.setTextFill(Color.BLACK);
        HBox hb = new HBox(c2w);
        hb.setPrefSize(500, 100);
        hb.setLayoutX(650);
        hb.setLayoutY(150);
        hb.setAlignment(Pos.CENTER);
        hb.setStyle("-fx-border-color:Black");
       // hb.setStyle("-fx-background-color:Black");
        /*
         * 2)
         * 
         */
        Button b1 = new Button("Core2web - Java");
        b1.setPrefSize(300,40);          //to change the size of the button
        b1.setStyle("-fx-background-color:Red");     //to change the bagckground color of the button box
        //b1.setTextFill(Color.RED);                         //to change the text color in the button box
        b1.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                
               // b1.setText("Core2web");
                b1.setStyle("-fx-background-color: Orange");  //to change the color of the button box after Clicking on the Button 
                System.out.println("Java 2024");

            }
            
        });
        HBox hb1 = new HBox(b1);
        hb1.setAlignment(Pos.CENTER);

      

        Button b2 = new Button("Core2web - SuperX");
        b2.setPrefSize(300,40); 
        b2.setStyle("-fx-background-color:Red");
        b2.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                
                //b2.setText("Core2web");
                b2.setStyle("-fx-background-color: White");
                System.out.println("Super-X 2024");

            }
            
        });
        HBox hb2 = new HBox(b2);
        hb2.setAlignment(Pos.CENTER);
      

        Button b3 = new Button("Core2web - DSA");
        b3.setPrefSize(300,40); 
        b3.setStyle("-fx-background-color:Red");
        b3.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                
                //b3.setText("Core2web");
                b3.setStyle("-fx-background-color: Green");
                System.out.println("DSA 2024");

            }
   
        });
        HBox hb3 = new HBox(b3);
        hb3.setAlignment(Pos.CENTER);

        VBox vb1 = new VBox(30,hb1,hb2,hb3);
        //vb1.setStyle("-fx-background-color:Black");  //to set the background color to VBox
        vb1.setAlignment(Pos.CENTER);
        vb1.setPrefSize(500, 500);  //size
        vb1.setLayoutX(650);
        vb1.setLayoutY(150);

       // vb1.setStyle("-fx-border-color:Black");
   
/*------------------------------------------------------------------------------------ */

        Button os = new Button("OS");
        os.setPrefSize(100, 20);
        os.setStyle("-fx-background-color:Black");
        os.setTextFill(Color.WHITE);
        os.setFont(new Font(20));
        VBox vbos = new VBox(os);
        vbos.setAlignment(Pos.CENTER);

        Button c = new Button("C");
        c.setPrefSize(100, 20);
        c.setStyle("-fx-background-color:Black");
        c.setTextFill(Color.WHITE);
        c.setFont(new Font(20));
        VBox vbc = new VBox(c);
        vbc.setAlignment(Pos.CENTER);

        Button cpp = new Button("CPP");
        cpp.setPrefSize(100, 20);
        cpp.setStyle("-fx-background-color:Black");
        cpp.setTextFill(Color.WHITE);
        cpp.setFont(new Font(20));
        VBox vbcpp = new VBox(cpp);
        vbcpp.setAlignment(Pos.CENTER);
        
        HBox hbone = new HBox(80,vbos,vbc,vbcpp);
        hbone.setAlignment(Pos.CENTER);
       // hbone.setStyle("-fx-background-color:White");
       

     

        Button java = new Button("JAVA");
        java.setPrefSize(200, 20);
        java.setStyle("-fx-background-color:Black");
        java.setTextFill(Color.WHITE);
        java.setFont(new Font(20));
        VBox vbjava = new VBox(java);
        vbjava.setAlignment(Pos.CENTER);

        Button dsa = new Button("DSA");
        dsa.setPrefSize(200, 20);
        dsa.setStyle("-fx-background-color:Black");
        dsa.setTextFill(Color.WHITE);
        dsa.setFont(new Font(20));
        VBox vbdsa = new VBox(dsa);
        vbdsa.setAlignment(Pos.CENTER);

        Button python = new Button("PYTHON");
        python.setPrefSize(200, 20);
        python.setStyle("-fx-background-color:Black");
        python.setTextFill(Color.WHITE);
        python.setFont(new Font(20));
        VBox vbpython = new VBox(python);
        vbpython.setAlignment(Pos.CENTER);
        
        HBox hbtwo = new HBox(60,vbjava,vbdsa,vbpython);
        hbtwo.setAlignment(Pos.CENTER);
       // hbtwo.setStyle("-fx-background-color:White");


        VBox box2 = new VBox(30,hbone,hbtwo);
        box2.setLayoutX(450);
        box2.setLayoutY(650);
        box2.setPrefHeight(200);
        box2.setPrefWidth(1000);
        box2.setAlignment(Pos.CENTER);
        box2.setStyle("-fx-background-color:White");
       // box2.setStyle("-fx-border-color:Black");
/*------------------------------------------------------------------------------------- */
        
        Group gr = new Group(hb,vb1,box2);

        Scene sc = new Scene(gr);
        sc.setFill(Color.PALETURQUOISE);

        primaryStage.setScene(sc);
        primaryStage.setTitle("Buttons_Assignment_2");
        
        primaryStage.setHeight(1000);
        primaryStage.setWidth(1800);
        primaryStage.setResizable(false);
        primaryStage.show();
    }
    
}
