package com.firebase.firebase_collections;

import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;

import org.json.JSONObject;

import com.firebase.controller.LoginController;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.UserRecord;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;

public class FirebaseService {

    private TextField emailField;
    private TextField passField;
    private LoginController loginController;

    public FirebaseService(LoginController loginController, TextField emailField, PasswordField passField){

        this.loginController = loginController;
        this.emailField = emailField;
        this.passField = passField;
    }
    
    public boolean signUp(){
        String email = emailField.getText();
        String password = passField.getText();

        try {
            
            UserRecord.CreateRequest request = new UserRecord.CreateRequest()
                .setEmail(email)
                .setPassword(password)
                .setDisabled(false);
            UserRecord userRecord = FirebaseAuth.getInstance().createUser(request);
            System.out.println("Successfully Created User :"+userRecord.getUid());
            showAlert("Success","User Created Successfully.");
            return true;


        } catch (Exception e) {
            e.printStackTrace();
            showAlert("Error","Failed to create user."+e.getMessage());
            return false;
        }
    }

  

    public boolean login(){

        String email = emailField.getText();
        String password = passField.getText();

        try {
            
            String apiKey = "AIzaSyDsIKPbpUvyE0LOY_7pd_0rkykEn0SYjhg";

            URL url = new URL("https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword?key=AIzaSyDsIKPbpUvyE0LOY_7pd_0rkykEn0SYjhg");
            HttpURLConnection conn = (HttpURLConnection)url.openConnection();
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Type", "application/json; charset-UTF-8");
            conn.setDoOutput(true);

            JSONObject jsonRequest = new JSONObject();
            jsonRequest.put("email", email);
            jsonRequest.put("password", password);
            jsonRequest.put("returnSecureToken", true);

            try(OutputStream os = conn.getOutputStream()){

                byte [] input = jsonRequest.toString().getBytes(StandardCharsets.UTF_8);
                os.write(input,0,input.length);
            }

            if (conn.getResponseCode()==200) {

                showAlert(true);
                return true;

            } else{

                showAlert("Invalid Login","Invalid Credential.........!");
                return false;
            }
        } catch (Exception e) {
            
            e.printStackTrace();
            showAlert(false);
            return false;

        }
    }

    private void showAlert(String tital, String message) {
        
        Alert alert  = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle(tital);
        alert.setHeaderText(null);
        alert.setContentText(message);
        alert.showAndWait();

    }

    private void showAlert(boolean isLogin) {
        
        Label msg = new Label("Wellcome");
        msg.setAlignment(Pos.CENTER);

        Button loggOutButton = new Button("Logout");

        VBox vb = new VBox(100,msg,loggOutButton);

        loggOutButton.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                loginController.initializeLoginScene();;
            }
            
        });

        Scene sc = new Scene(vb,600,300);
        loginController.setpStageScene(sc);
    }
}
