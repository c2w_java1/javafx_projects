package com.firebase.controller;

import java.io.FileInputStream;

import com.firebase.firebase_collections.FirebaseService;
import com.google.api.services.storage.Storage.Projects.HmacKeys.Create;
import com.google.auth.oauth2.GoogleCredentials;
import com.google.firebase.FirebaseApp;
//import com.google.firebase.internal.FirebaseService;
//import com.google.protobuf.DescriptorProtos.FileOptions;
import com.google.firebase.FirebaseOptions;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class LoginController extends Application {

    private Stage pStage;
    private FirebaseService firebaseService;
    private Scene sc;

    public void setpStageScene (Scene sc){

        pStage.setTitle("Firebase_Demo");
        
        pStage.setScene(sc);
    }

    public void initializeLoginScene(){

            Scene logiScene = createLoginScene();
            this.sc = logiScene;
            pStage.setScene(logiScene); 
            
    }

    
    @SuppressWarnings("deprecation")
    @Override
    public void start(Stage pStage) throws Exception {
        this.pStage = pStage;

        try {
            FileInputStream serviceAccount = new FileInputStream("src/main/resources/firebase-demo.json");
            FirebaseOptions operations = new FirebaseOptions.Builder()
                .setCredentials(GoogleCredentials.fromStream(serviceAccount))
                .setDatabaseUrl("https://fir-demo-da40a-default-rtdb.asia-southeast1.firebasedatabase.app/")
                .build();
            FirebaseApp.initializeApp(operations);
        } catch (Exception e) {
            e.printStackTrace();
        }

        Scene sc = createLoginScene();
        this.sc = sc;
        pStage.setTitle("Firebase");
        pStage.setScene(sc);
        pStage.show();
    }

    @SuppressWarnings("rawtypes")
    private Scene createLoginScene() {
        
        TextField email = new TextField();
        email.setPromptText("Email");

        PasswordField pass = new PasswordField();
        pass.setPromptText("Password");

        Button signUpButton = new Button("Sign Up");
        Button loginButton = new Button("Login");

        firebaseService = new FirebaseService(this,email,pass);

        signUpButton.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                
                firebaseService.signUp();
            }
            
        });

        loginButton.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                firebaseService.login();
            }
            
        });

        VBox fileBox = new VBox(20,email,pass);
        HBox buttonBox = new HBox(20,loginButton,signUpButton);
        VBox comBox = new VBox(10,fileBox,buttonBox);

        Pane view = new Pane(comBox);

        return new Scene(view,600,300);
    }
    
    
}
