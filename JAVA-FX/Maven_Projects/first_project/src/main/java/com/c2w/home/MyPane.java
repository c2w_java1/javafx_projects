package javaFiles.home;

import javafx.application.Application;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.Border;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.stage.Stage;

public class MyPane extends Application {

    @Override
    public void start(Stage primaryStage) {
/*       
        Label lb = new Label("Good Morning...!");
        lb.setFont(new Font(20));
        //lb.setAlignment(Pos.CENTER);

        Pane p = new Pane();
        p.getChildren().addAll(lb);
        p.setLayoutX(300);
        p.setLayoutY(20);
*/
        Label lb1 = new Label("Hello Core2web");
        lb1.setFont(new Font(30));
        //lb1.setAlignment(Pos.TOP_CENTER);

        Label lb2 = new Label("Hello Binecaps");
        lb2.setFont(new Font(30));

        Label lb3 = new Label("Hello Inqbitors");
        lb3.setFont(new Font(30));
    
        Label lb4 = new Label("Nikhil");
        lb4.setFont(new Font(30));


        
        BorderPane b = new BorderPane();
        b.setTop(lb1);
        b.setBottom(lb2);
        b.setRight(lb3);
        b.setCenter(lb4);
        
        Scene sc = new Scene( b,500,500);
//      sc.setFill(Color.CYAN);
        primaryStage.setTitle("New Stage MYPANE");
        primaryStage.setScene(sc);
        primaryStage.setResizable(false);
        primaryStage.setHeight(800);
        primaryStage.setWidth(1000);
        primaryStage.show();
    }


}
