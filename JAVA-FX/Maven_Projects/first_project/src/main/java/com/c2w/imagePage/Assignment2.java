package com.c2w.imagePage;

import javafx.application.Application;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.stage.Stage;

public class Assignment2 extends Application{

    @Override
    public void start(Stage primaryStage){

        
        Label lb1 = new Label("Java");
        lb1.setFont(new Font(50));
        VBox vbJava = new VBox(lb1);
        vbJava.setAlignment(Pos.CENTER);
        vbJava.setPrefWidth(500);

        Image igJava = new Image("assets/image/java.png");
        ImageView ivJava = new ImageView(igJava);
        ivJava.setFitHeight(200);
        ivJava.setFitWidth(300);
        //ivJava.setPreserveRatio(true);

        HBox hb1 = new HBox(ivJava,vbJava);
        hb1.setStyle("-fx-background-Color:SkyBlue");
        hb1.setPrefSize(847.5, 200);


        Label lb2 = new Label("Python");
        lb2.setFont(new Font(50));
        VBox vbPython = new VBox(lb2);
        vbPython.setAlignment(Pos.CENTER);
        vbPython.setPrefWidth(500);

        Image igPython = new Image("assets/image/python.png");
        ImageView ivPython = new ImageView(igPython);
        ivPython.setFitHeight(200);
        ivPython.setFitWidth(300);

        HBox hb2 = new HBox(ivPython,vbPython);
        hb2.setStyle("-fx-background-Color:ORANGE");
        hb2.setPrefSize(847.5, 200);


        Label lb3 = new Label("C");
        lb3.setFont(new Font(50));
        VBox vbC = new VBox(lb3);
        vbC.setAlignment(Pos.CENTER);
        vbC.setPrefWidth(500);

        Image igC = new Image("assets/image/c.png");
        ImageView ivC = new ImageView(igC);
        ivC.setFitHeight(200);
        ivC.setFitWidth(300);

        HBox hb3 = new HBox(ivC,vbC);
        hb3.setStyle("-fx-background-Color:GREEN");
        hb3.setPrefSize(847.5, 200);


        Label lb4 = new Label("CPP");
        lb4.setFont(new Font(50));
        VBox vbCpp = new VBox(lb4);
        vbCpp.setAlignment(Pos.CENTER);
        vbCpp.setPrefWidth(500);

        Image igCpp = new Image("assets/image/cpp.png");
        ImageView ivCpp = new ImageView(igCpp);
        ivCpp.setFitHeight(200);
        ivCpp.setFitWidth(300);

        HBox hb4 = new HBox(ivCpp,vbCpp);
        hb4.setStyle("-fx-background-Color:GOLD");
        hb4.setPrefSize(847.5, 200);


        VBox vb1 = new VBox(35,hb1,hb2,hb3,hb4);
        vb1.setLayoutX(35);
        vb1.setLayoutY(32);

//---------------------------------------------------------------------//

        Label lb5 = new Label("Spring");
        lb5.setFont(new Font(50));
        VBox vbSp = new VBox(lb5);
        vbSp.setAlignment(Pos.CENTER);
        vbSp.setPrefWidth(500);

        Image igSp = new Image("assets/image/spring.png");
        ImageView ivSp = new ImageView(igSp);
        ivSp.setFitHeight(200);
        ivSp.setFitWidth(300);
        

        HBox hb5 = new HBox(ivSp,vbSp);
        hb5.setStyle("-fx-background-Color:SkyBlue");
        hb5.setPrefSize(847.5, 200);


        Label lb6 = new Label("React");
        lb6.setFont(new Font(50));
        VBox vbReact = new VBox(lb6);
        vbReact.setAlignment(Pos.CENTER);
        vbReact.setPrefWidth(500);

        Image igReact = new Image("assets/image/react.png");
        ImageView ivReact = new ImageView(igReact);
        ivReact.setFitHeight(200);
        ivReact.setFitWidth(300);

        HBox hb6 = new HBox(ivReact,vbReact);
        hb6.setStyle("-fx-background-Color:ORANGE");
        hb6.setPrefSize(847.5, 200);


        Label lb7 = new Label("Flutter");
        lb7.setFont(new Font(50));
        VBox vbFlutter = new VBox(lb7);
        vbFlutter.setAlignment(Pos.CENTER);
        vbFlutter.setPrefWidth(500);

        Image igFlutter = new Image("assets/image/flutter.png");
        ImageView ivFlutter = new ImageView(igFlutter);
        ivFlutter.setFitHeight(200);
        ivFlutter.setFitWidth(300);

        HBox hb7 = new HBox(ivFlutter,vbFlutter);
        hb7.setStyle("-fx-background-Color:GREEN");
        hb7.setPrefSize(847.5, 200);


        Label lb8 = new Label("NodeJS");
        lb8.setFont(new Font(50));
        VBox vbNode = new VBox(lb8);
        vbNode.setAlignment(Pos.CENTER);
        vbNode.setPrefWidth(500);

        Image igNode = new Image("assets/image/node.png");
        ImageView ivNode = new ImageView(igNode);
        ivNode.setFitHeight(200);
        ivNode.setFitWidth(300);

        HBox hb8 = new HBox(ivNode,vbNode);
        hb8.setStyle("-fx-background-Color:GOLD");
        hb8.setPrefSize(847.5, 200);


        VBox vb2 = new VBox(35,hb5,hb6,hb7,hb8);
        vb2.setLayoutX(917.5);
        vb2.setLayoutY(32);


        Group gr = new Group(vb1,vb2);

        Scene sc = new Scene(gr);
        sc.setFill(Color.BLACK);

        primaryStage.setScene(sc);
        primaryStage.setTitle("MyAssignment2");
        
        primaryStage.setHeight(1000);
        primaryStage.setWidth(1800);
        primaryStage.setResizable(false);
        primaryStage.show();
       
    }
    
}