package com.c2w.imagePage;

import javafx.application.Application;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.stage.Stage;

public class ImagePage extends Application {

    @Override
    public void start(Stage prStage) throws Exception {
        
        Image ig = new Image("image/shivajiRaje.jpg");
        ImageView iv = new ImageView(ig);
        iv.setPreserveRatio(false);
        iv.setFitHeight(300);
        iv.setFitWidth(900);

        Label lb = new Label("Shivaji Maharaj");
        lb.setFont(new Font(30));
        lb.setTextFill(Color.BLACK);

        VBox vb = new VBox(lb);
        vb.setAlignment(Pos.CENTER);
    
        HBox hb = new HBox(iv,vb);
        hb.setLayoutX(20);
        hb.setLayoutY(30);
        hb.setAlignment(Pos.CENTER);
        hb.setStyle("-fx-background-color:ORANGE");

        Group gr = new Group(hb);

        Scene sc = new Scene(gr);

        sc.setFill(Color.BLACK);
        prStage.setScene(sc);
        prStage.setTitle("ImagePageWindow");
        prStage.setHeight(1000);
        prStage.setWidth(1800);
        prStage.setResizable(false);
        prStage.show();
    } 
}
