package com.c2w.myButtons;



import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.Stage;



public class PracticeSheet1 extends Application {

    @Override
    public void start(Stage primaryStage) {
       /*  
        Button jan = new Button("January");
        jan.setPrefSize(200, 40);
        jan.setStyle("-fx-background-color:Blue");
        jan.setFont(new Font(20));
        jan.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                jan.setStyle("-fx-background-color:White");
                System.out.println("January");
            }
            
        });
        
        Button feb = new Button("February");
        feb.setPrefSize(200, 40);
        feb.setStyle("-fx-background-color:Blue");
        feb.setFont(new Font(20));
        feb.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                feb.setStyle("-fx-background-color:White");
                System.out.println("February");
            }
            
        });

        Button mar = new Button("March");
        mar.setPrefSize(200, 40);
        mar.setStyle("-fx-background-color:Blue");
        mar.setFont(new Font(20));
        mar.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                mar.setStyle("-fx-background-color:White");
                System.out.println("March");
            }
            
        });

        Button apr = new Button("April");
        apr.setPrefSize(200, 40);
        apr.setStyle("-fx-background-color:Blue");
        apr.setFont(new Font(20));
        apr.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                apr.setStyle("-fx-background-color:White");
                System.out.println("April");
            }
            
        });

        Button may = new Button("May");
        may.setPrefSize(200, 40);
        may.setStyle("-fx-background-color:Blue");
        may.setFont(new Font(20));
        may.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                may.setStyle("-fx-background-color:White");
                System.out.println("May");
            }
            
        });

        Button june = new Button("June");
        june.setPrefSize(200, 40);
        june.setStyle("-fx-background-color:Blue");
        june.setFont(new Font(20));
        june.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                june.setStyle("-fx-background-color:White");
                System.out.println("June");
            }
            
        });

        VBox vb1 = new VBox(30,jan,feb,mar,apr,may,june);
        vb1.setAlignment(Pos.CENTER);
        vb1.setPrefSize(300, 500);


        Button july = new Button("July");
        july.setPrefSize(200, 40);
        july.setStyle("-fx-background-color:Blue");
        july.setFont(new Font(20));
        july.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                july.setStyle("-fx-background-color:White");
                System.out.println("July");
            }
            
        });

        Button aug = new Button("August");
        aug.setPrefSize(200, 40);
        aug.setStyle("-fx-background-color:Blue");
        aug.setFont(new Font(20));
        aug.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                aug.setStyle("-fx-background-color:White");
                System.out.println("August");
            }
            
        });

        Button sep = new Button("September");
        sep.setPrefSize(200, 40);
        sep.setStyle("-fx-background-color:Blue");
        sep.setFont(new Font(20));
        sep.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                sep.setStyle("-fx-background-color:White");
                System.out.println("September");
            }
            
        });

        Button aut = new Button("October");
        aut.setPrefSize(200, 40);
        aut.setStyle("-fx-background-color:Blue");
        aut.setFont(new Font(20));
        aut.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                aut.setStyle("-fx-background-color:White");
                System.out.println("October");
            }
            
        });

        Button nov = new Button("November");
        nov.setPrefSize(200, 40);
        nov.setStyle("-fx-background-color:Blue");
        nov.setFont(new Font(20));
        nov.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                nov.setStyle("-fx-background-color:White");
                System.out.println("November");
            }
            
        });

        Button dec = new Button("December");
        dec.setPrefSize(200, 40);
        dec.setStyle("-fx-background-color:Blue");
        dec.setFont(new Font(20));
        dec.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                dec.setStyle("-fx-background-color:White");
                System.out.println("December");
            }
            
        });

        VBox vb2 = new VBox(30,july,aug,sep,aut,nov,dec);
        vb2.setAlignment(Pos.CENTER);
        vb2.setPrefSize(300, 500);

        HBox hb = new HBox(50,vb1,vb2);
        hb.setAlignment(Pos.CENTER);
        hb.setLayoutX(600);
        hb.setLayoutY(250);
        
*/
/* -----------------------------------------------------------------------------------------------------------------*

        TextField tx = new TextField();
        tx.setPrefSize(1800, 50);
        
        Button printText = new Button("Print Text");
        printText.setPrefSize(200, 0);
        Label print = new Label();
        print.setFont(new Font(50)); 
        print.setTextFill(Color.RED);
        print.setLayoutX(30);
        printText.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                
                System.out.println(tx.getText());
                print.setText(tx.getText());

            }
            
        });

       
        HBox hb1 = new HBox(tx);
        HBox hb2 = new HBox(printText);
        HBox hbPrint = new HBox(print);

        VBox vb = new VBox(50,hb1,hb2,hbPrint);
        vb.setPrefSize(1700, 50);
        vb.setLayoutY(400);
        vb.setLayoutX(50);
        */
/* 
//----------------------Square & Square Root Of Number -------------------------------------//
        TextField tx = new TextField();
        tx.setPrefSize(400, 50);
        tx.setAlignment(Pos.CENTER);
        
       // tx.setLayoutY(500);

        Label print = new Label();
        print.setFont(new Font(30)); 
        print.setTextFill(Color.BLACK);
        print.setAlignment(Pos.CENTER);

        Button square = new Button("Square");
        square.setPrefSize(200, 50);
        square.setAlignment(Pos.CENTER);
        square.setTextFill(Color.WHITE);
        square.setStyle("-fx-background-color:Red");
        square.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                
                int num = Integer.parseInt(tx.getText()) ; //
                double result = num*num;
                System.out.println(result);
                print.setText("Squre of "+num +" is ==>"+Double.toString(result)); //conver Double value to String and Print on Scene
                //print.setText(Integer.toString(result));
              //  print.setText(result);

            }
            
        });

        Button squareRoot = new Button("Square Root");
        squareRoot.setPrefSize(200, 50);
        squareRoot.setAlignment(Pos.CENTER);
        squareRoot.setTextFill(Color.WHITE);
        squareRoot.setStyle("-fx-background-color:Red");
        squareRoot.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                
                int num = Integer.parseInt(tx.getText()) ;
                double result = Math.sqrt(num); 
                System.out.println(result);
                print.setText("Squre Root of "+num +" is ==>"+Double.toString(result));

            }
            
        });


       
        HBox hb1 = new HBox(tx);
        hb1.setAlignment(Pos.CENTER);
        hb1.setPrefSize(500,200 );
        //hb1.setLayoutY(400);
        //hb1.setStyle("-fx-background-color:Black");

        HBox hb2 = new HBox(square);
        hb2.setAlignment(Pos.CENTER);
        hb2.setPrefSize(500, 50);
        //hb2.setStyle("-fx-background-color:Black");

        HBox hb3 = new HBox(squareRoot);
        hb3.setAlignment(Pos.CENTER);
        hb3.setPrefSize(500, 50);

        HBox hbPrint = new HBox(print);
        hbPrint.setAlignment(Pos.CENTER);

        VBox vb = new VBox(20,hb1,hb2,hb3,hbPrint);
        vb.setPrefSize(500, 500);
        vb.setLayoutY(200);
        vb.setLayoutX(650);  
        vb.setStyle("-fx-border-color:Black");     
*/
/*
 * 
 
//---------------------------AUTHONTICATION  (UserName & Password verification)-----------------------------------//

        Label name = new Label("User Name");
        TextField id = new TextField();
        id.setFont(new Font(20));
        id.setPrefSize(400, 50);
        
        Label password = new Label("Password");
        PasswordField pass = new PasswordField();
        pass.setFont(new Font(20));
      
        Button login = new Button("Login");
        login.setAlignment(Pos.CENTER);
        login.setTextFill(Color.WHITE);
        login.setFont(new Font(20));
        login.setPrefSize(200, 0);
        login.setStyle("-fx-background-color:Black");
        Label show = new Label();
        show.setFont(new Font(20));
        login.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                String n = id.getText();
                String p = pass.getText();

                if( n.equals("Nikhil") && p.equals("1234")){

                    System.out.println("Welcome......!");
                    show.setText("Welcome.....!");
                } else {

                    System.out.println("Wrong Password plese try again");
                    show.setText("Wrong Password plese try again");
                }
            }
            
        });
        HBox button = new HBox(50,login);
        button.setAlignment(Pos.CENTER);

        VBox vbLogin = new VBox(30,name,id,password,pass,button,show);
        
        vbLogin.setPrefSize(400, 500);
        vbLogin.setLayoutX(700);
        vbLogin.setLayoutY(250);
*/

//-----------------------------String is Palindrome or not------------------------------------------//
        TextField tx = new TextField();
        tx.setFont(new Font(20));
        tx.setPrefSize(400, 50);

        Button bt = new Button("Check Palindrome");
        bt.setAlignment(Pos.CENTER);
        bt.setTextFill(Color.WHITE);
        bt.setFont(new Font(20));
        bt.setPrefSize(200, 0);
        bt.setStyle("-fx-background-color:Black");

        Label show = new Label();
        show.setFont(new Font(30));
        show.setAlignment(Pos.CENTER);
        bt.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                
                String s = tx.getText();
                StringBuilder sb = new StringBuilder(s);
                sb.reverse();
                String sr = sb.toString();
                if(sr.equals(s)){
                    show.setText("Given String is Palindrome");
                }else{
                    show.setText("Given String is not-Palindrome");
                }
                
            }
            
        });
        
         VBox vb = new VBox(30,tx,bt,show);
         vb.setAlignment(Pos.CENTER);
         vb.setPrefSize(500, 500);

         vb.setLayoutX(650);
         vb.setLayoutY(250);
         
 
/* 
//-----------------------------  Scene Color Change -------------------- //
        
        Button bt = new Button("Click Here !");
        bt.setAlignment(Pos.CENTER);
        bt.setStyle("-fx-background-color:Pink");
        bt.setStyle("-fx-font-color:Black");
        bt.setPrefSize(200, 50);
        bt.setLayoutX(800);
        bt.setLayoutY(400);
        HBox hb = new HBox();
        hb.setStyle("-fx-background-color:White");
        hb.setPrefSize(1800, 1000);
        
        bt.setOnAction(new EventHandler<ActionEvent>() {
            int i =1;
            @Override
            public void handle(ActionEvent event) {
                    
                    switch (i) {
                        case 1:
                            hb.setStyle("-fx-background-color:Red");
                            i++;
                            break;

                        case 2:
                            hb.setStyle("-fx-background-color:Orange");
                            i++;
                            break;

                        case 3:
                            hb.setStyle("-fx-background-color:Pink");
                            i++;
                            break;
                    
                        case 4:
                            hb.setStyle("-fx-background-color:Green");
                            i++;
                            break;

                        case 5:
                            hb.setStyle("-fx-background-color:Blue");
                            i++;
                            break;

                        case 6:
                            hb.setStyle("-fx-background-color:Aqua");
                            i++;
                            break;

                        case 7:
                            hb.setStyle("-fx-background-color:Black");
                            i=1;
                            break;
                          
            
                    }
                }
            
        });
    */
        Group gr = new Group(vb);
        Scene sc = new Scene(gr);
        sc.setFill(Color.YELLOWGREEN);
        primaryStage.setScene(sc);
        primaryStage.setTitle("Practice_Sheet_1");
        primaryStage.setResizable(false);
        primaryStage.setHeight(1000);
        primaryStage.setWidth(1800);
        primaryStage.show();
    }

}
