package com.example.dashbord;

import com.example.controller.LoginController;
import com.example.firebaseCoding.DataService;

import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;
import javafx.scene.layout.HBox;
import javafx.scene.text.Text;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;

public class AdminPage {

    private final DataService dataService;
    private final LoginController loginController;
    private VBox adminDashboard;

    public AdminPage(LoginController loginController, DataService dataService) {
    
        this.loginController = loginController;
        this.dataService = dataService;
    }

    public VBox createAdminDashboard(Runnable logoutHandler) {

        VBox vbox = new VBox();
        vbox.setPadding(new Insets(10));
        vbox.setSpacing(10);

        Button logoutButton = new Button("Logout");
        logoutButton.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {

                logoutHandler.run();
            }
            
        });

        adminDashboard = new VBox();
        adminDashboard.setSpacing(10);
        adminDashboard.getChildren().addAll(loadProjectDetails());
        adminDashboard.getChildren().add(logoutButton);
        vbox.getChildren().addAll(logoutButton,adminDashboard);

        return vbox;

    }

    private VBox loadProjectDetails()  {
        
        VBox vbox = new VBox();
        vbox.setSpacing(10);

        try {
            vbox.getChildren().clear();
            List<Map<String, Object>> projectDetails =dataService.getDataInDescendingOrder("collectionName", "timestamp");
            for(Map<String, Object> projectDetail : projectDetails){

                HBox projectCard = createProjectCard(projectDetail);
                vbox.getChildren().add(projectCard);
            }
        } catch (ExecutionException | InterruptedException ex) {
            ex.printStackTrace();
        }
        return vbox;

    }

    private HBox createProjectCard(Map<String, Object> projectDetail) {
        HBox card = new HBox();
        card.setPadding(new Insets(10));
        card.setSpacing(10);
        card.setStyle("-fx-border-color: black; -fx-border-width: 1; -fx-background-color:#f0f0f0;");

        VBox projectVBox = new VBox();
        projectVBox.setSpacing(5);

        VBox membersVBox = new VBox();
        membersVBox.setSpacing(5);


        if (projectDetail != null) {
            if (projectDetail.get("leaderName") != null) {
                Text leaderText = new Text("Leader name: ");
                leaderText.setStyle("-fx-font-weight: bold; -fx-font-size: 12;");
                Label leaderLabel = new Label(projectDetail.get("leaderName").toString());
                HBox leaderBox = new HBox(leaderText, leaderLabel);
                leaderBox.setSpacing(5); // Add spacing between text and label
                projectVBox.getChildren().add(leaderBox);
            }

            if (projectDetail.get("groupName") != null) {
                Text groupText = new Text("Group name: ");
                groupText.setStyle("-fx-font-weight: bold; -fx-font-size: 12;");
                Label groupLabel = new Label(projectDetail.get("groupName").toString());
                HBox groupBox = new HBox(groupText, groupLabel);
                groupBox.setSpacing(5); // Add spacing between text and label
                projectVBox.getChildren().add(groupBox);
            }

            if (projectDetail.get("projectName") != null) {
                
                Text projectText = new Text("Project: ");
                projectText.setStyle("-fx-font-weight: bold; -fx-font-size: 12;");
                Label projectLabel = new Label(projectDetail.get("projectName").toString());
                HBox projectBox = new HBox(projectText, projectLabel);
                projectBox.setSpacing(5); // Add spacing between text and label
                projectVBox.getChildren().add(projectBox);
            }

            for (int i = 1; i <= 4; i++) {
                
                String memberKey = "member" + i;
                if (projectDetail.get(memberKey) != null) {
                
                    Text memberText = new Text("Member " + i + ": ");
                    memberText.setStyle("-fx-font-weight: bold; -fx-font-size: 12;");
                    Label memberLabel = new Label(projectDetail.get(memberKey).toString());
                    HBox memberBox = new HBox(memberText, memberLabel);
                    memberBox.setSpacing(5); // Add spacing between text and label
                    membersVBox.getChildren().add(memberBox);
                }
            }
        }


        Button editButton = new Button("Edit");
        Button deleteButton = new Button("Delete");

        editButton.setOnAction(new EventHandler<ActionEvent>() {
           
            @Override
            public void handle(ActionEvent event) {
            handleEdit(projectDetail);
            }
        });

        deleteButton.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                handleDelete(projectDetail, card);
            }

            
        });

        VBox buttonsVBox = new VBox(5, editButton, deleteButton);
        card.getChildren().addAll(projectVBox, membersVBox, buttonsVBox);
        return card;

            
    }

    protected void handleEdit(Map<String, Object> projectDetail) {
        if (loginController != null) {
            VBox editScene = createUserScene(projectDetail, () -> {
            adminDashboard.getChildren().clear(); // Clear existing data in

            adminDashboard.getChildren().addAll(loadProjectDetails());
            loginController.returnToAdminView();
        });
            
        loginController.setPrimaryStage(new Scene(editScene, 700, 700));
        } else {
        
            System.err.println("Login controller is not initialized.");
            // Handle the situation where loginController is null
        }
    }

    private void handleDelete(Map<String, Object> projectDetail, HBox card) {
    
        try {
            
            dataService.deleteProject("collectionName",projectDetail.get("leaderName").toString());
            VBox parentVBox = (VBox) card.getParent();
            parentVBox.getChildren().remove(card);

        } catch (ExecutionException | InterruptedException ex) {
            
            ex.printStackTrace();
        }
    }

    private VBox createUserScene(Map<String, Object> projectDetail, Runnable returnToAdminView) {

        Label groupLabel = new Label("Enter group name:");
        groupLabel.setStyle("-fx-font-size:12;-fx-font-weight:bold");
        TextField groupName = new TextField((String) projectDetail.getOrDefault("groupName", ""));
        groupName.setPromptText("Group name");
        VBox groupBox = new VBox(10, groupLabel, groupName);
        groupBox.setMaxSize(300, 20);
        Label projectLabel = new Label("Enter project name:");
        projectLabel.setStyle("-fx-font-size:12;-fx-font-weight:bold");

        TextField projectName = new TextField((String) projectDetail.getOrDefault("projectName", ""));
        projectName.setPromptText("Project name");
        VBox projectBox = new VBox(10, projectLabel, projectName);
        projectBox.setMaxSize(300, 20);
        Label mobileNumber = new Label("Enter mobile number:");
        mobileNumber.setStyle("-fx-font-size:12;-fx-font-weight:bold");
        TextField mobTextField = new TextField((String)projectDetail.getOrDefault("mobileNum", ""));

        mobTextField.setPromptText("Mobile number");
        VBox mobBox = new VBox(10, mobileNumber, mobTextField);
        mobBox.setMaxSize(300, 20);

        Label leaderLabel = new Label("Enter leader name:");

        leaderLabel.setStyle("-fx-font-size:12;-fx-font-weight:bold");
        TextField leaderName = new TextField((String)projectDetail.getOrDefault("leaderName", ""));
        leaderName.setPromptText("Leader name");
        VBox leaderNameBox = new VBox(10, leaderLabel, leaderName);

        leaderNameBox.setMaxSize(300, 20);

        Label memberLabel1 = new Label("Enter member 1 name:");
        memberLabel1.setStyle("-fx-font-size:12;-fx-font-weight:bold");
        TextField member1 = new TextField((String) projectDetail.getOrDefault("member1", ""));

        member1.setPromptText("Member 1 name");
        VBox mem1Box = new VBox(10, memberLabel1, member1);
        mem1Box.setMaxSize(300, 20);

        Label memberLabel2 = new Label("Enter member 2 name:");
        memberLabel2.setStyle("-fx-font-size:12;-fx-font-weight:bold");
        TextField member2 = new TextField((String)projectDetail.getOrDefault("member2", ""));
        member2.setPromptText("Member 2 name");
        VBox mem2Box = new VBox(10, memberLabel2, member2);
        mem2Box.setMaxSize(300, 20);

        Label memberLabel3 = new Label("Enter member 3 name:");
        memberLabel3.setStyle("-fx-font-size:12;-fx-font-weight:bold");
        TextField member3 = new TextField((String)projectDetail.getOrDefault("member3", ""));
        member3.setPromptText("Member 3 name");
        VBox mem3Box = new VBox(10, memberLabel3, member3);
        mem3Box.setMaxSize(300, 20);

        Button updateButton = new Button("Update Data");
        updateButton.setStyle("-fx-font-size:12;-fx-font-weight:bold");
        HBox buttonBox = new HBox(updateButton);

        buttonBox.setAlignment(Pos.CENTER);
        Button returnButton = new Button("Return to Admin View");
        returnButton.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                
                returnToAdminView.run();
            }
            
        });

        VBox dataBox = new VBox(25, groupBox, projectBox, mobBox, leaderNameBox, mem1Box, mem2Box, mem3Box, buttonBox, returnButton);
            dataBox.setAlignment(Pos.CENTER);
            updateButton.setOnAction(new EventHandler<ActionEvent>() {

                @Override
                public void handle(ActionEvent event) {
                    
                    Map<String, Object> updatedData = new HashMap<>();
                    updatedData.put("groupName", groupName.getText());
                    updatedData.put("projectName", projectName.getText());
                    updatedData.put("leaderName", leaderName.getText());
                    updatedData.put("mobileNum", mobTextField.getText());
                    updatedData.put("member1", member1.getText());
                    updatedData.put("member2", member2.getText());
                    updatedData.put("member3", member3.getText());
                    updatedData.put("timestamp", LocalDateTime.now());
               
                
            

            try {
                dataService.updateData("collectionName",
                projectDetail.get("leaderName").toString(), updatedData);
                } catch (ExecutionException | InterruptedException ex) {
                
                    ex.printStackTrace();
                }


                adminDashboard.getChildren().clear();
                adminDashboard.getChildren().addAll(loadProjectDetails());

                }
            });

            return dataBox;
    }

}
