package com.example.dashbord;


import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutionException;

import com.example.controller.LoginController;
import com.example.firebaseCoding.DataService;
import com.google.cloud.firestore.DocumentSnapshot;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Labeled;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;


public class UserPage {
    
    private DataService dataService;
    private Label dataMsg;
   
    public UserPage(DataService dataService) {
        this.dataService = dataService;
    }

    public VBox createUserScene(Runnable logoutHandler) { 
        
        dataMsg = new Label();

        Label groupLabel = new Label("Enter group name:");
        groupLabel.setStyle("-fx-font-size:12;-fx-font-weight:bold");
        TextField groupName = new TextField();
        groupName.setPromptText("Project name");
        VBox groupBox = new VBox(10, groupLabel, groupName);
        groupBox.setMaxSize(300, 20);
        Label projectLabel = new Label("Enter project name:");
        projectLabel.setStyle("-fx-font-size:12;-fx-font-weight:bold");
        TextField projectName = new TextField();
        projectName.setPromptText("Project name");
        VBox projectBox = new VBox(10, projectLabel, projectName);
        projectBox.setMaxSize(300, 20);


        Label mobileNumber = new Label("Enter mobile number:");
        mobileNumber.setStyle("-fx-font-size:12;-fx-font-weight:bold");
        TextField mobTextField = new TextField();
        mobTextField.setPromptText("Leader name");
        VBox mobBox = new VBox(10, mobileNumber, mobTextField);
        mobBox.setMaxSize(300, 20);
        Label leaderLabel = new Label("Enter leader name:");
        leaderLabel.setStyle("-fx-font-size:12;-fx-font-weight:bold");
        TextField leaderName = new TextField();
        leaderName.setPromptText("Leader name");
        VBox leaderNameBox = new VBox(10, leaderLabel,leaderName);
        leaderNameBox.setMaxSize(300, 20);
        Label memberLabel1 = new Label("Enter member 1 name:");
        memberLabel1.setStyle("-fx-font-size:12;-fx-font-weight:bold");
        TextField member1 = new TextField();
        member1.setPromptText("Member 1 name");
        VBox mem1Box = new VBox(10, memberLabel1, member1);
        mem1Box.setMaxSize(300, 20);

        Label memberLabel2 = new Label("Enter member 2 name:");
        memberLabel2.setStyle("-fx-font-size:12;-fx-font-weight:bold");
        TextField member2 = new TextField();
        member2.setPromptText("Member 2 name");
        VBox mem2Box = new VBox(10, memberLabel2, member2);
        mem2Box.setMaxSize(300, 20);
        Label memberLabel3 = new Label("Enter member 3 name:");
        memberLabel3.setStyle("-fx-font-size:12;-fx-font-weight:bold");
        TextField member3 = new TextField();

        member3.setPromptText("member 3 name");
        VBox mem3Box = new VBox(10, memberLabel3, member3);
        mem3Box.setMaxSize(300, 20);

        Button addButton = new Button("Add Data");
        addButton.setStyle("-fx-font-size:12;-fx-font-weight:bold");
        HBox buttonBox = new HBox(addButton);
        buttonBox.setAlignment(Pos.CENTER);
        Button logoutButton = new Button("Logout");

        addButton.setOnAction(new EventHandler<ActionEvent>() {
        @Override
        public void handle(ActionEvent event) {
        Map<String, Object> data = new HashMap<>();
            data.put("groupName", groupName.getText());
            data.put("projectName", projectName.getText());
            data.put("leaderName", leaderName.getText());
            data.put("mobileNum", mobTextField.getText());
            data.put("member2", member1.getText());
            data.put("member3", member2.getText());
            data.put("member4", member3.getText());
            data.put("timestamp", LocalDateTime.now());
            
            try {
            
                dataService.addData("collectionName",leaderName.getText(), data);

                dataMsg.setText("Added successfully"); // Update status

                groupName.clear();
                projectName.clear();
                leaderName.clear();
                mobTextField.clear();
                member1.clear();
                member2.clear();
                member3.clear();
            } catch (ExecutionException | InterruptedException ex) {
                dataMsg.setText("Something went wrong"); 

                ex.printStackTrace();
            }
         }
    });

    logoutButton.setOnAction(new EventHandler<ActionEvent>() {
        @Override
            public void handle(ActionEvent event) {
            logoutHandler.run(); // Execute logout handler
        }
    });

    Text message = getTheLoginName();

    VBox vButton = new VBox(logoutButton);
    logoutButton.setAlignment(Pos.TOP_LEFT);

    VBox dataBox = new VBox(25, vButton, message, groupBox, projectBox, mobBox, leaderNameBox, mem1Box, mem2Box, mem3Box, buttonBox, dataMsg);
    dataBox.setAlignment(Pos.CENTER);
    return dataBox;
    }

    private Text getTheLoginName() {
        Label dataLabel = new Label();
        try {
            String key = LoginController.key;
            System.out.println("Value of key: " + key);
            DocumentSnapshot dataObject = dataService.getData("users", key);
            String userName = dataObject.getString("username");
            System.out.println("Username fetched: " + userName);
            dataLabel.setText(userName);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    
        return new Text("Welcome " + dataLabel.getText());
    }

}
