package com.example.controller;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Scene;

import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import java.util.HashMap;
import java.util.Map;

import com.example.firebaseCoding.DataService;


public class SignupController {

    private LoginController loginController;

    public SignupController(LoginController loginController) {
        this.loginController = loginController;

    }

    public Scene createSignupScene(Stage primaryStage) {
        Label userLabel = new Label("Username:");
        TextField userTextField = new TextField();
        Label passLabel = new Label("Password:");
        PasswordField passField = new PasswordField();
        Button signupButton = new Button("Signup");
        VBox fieldBox1 = new VBox(10, userLabel,userTextField);
        fieldBox1.setMaxSize(300, 30);
        VBox fieldBox2 = new VBox(10, passLabel,passField);
        fieldBox2.setMaxSize(300, 30);
        HBox buttonBox = new HBox(50,signupButton);
        buttonBox.setMaxSize(350, 30);
        buttonBox.setAlignment(Pos.CENTER);
        signupButton.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                handleSignup(primaryStage,userTextField.getText(), passField.getText());
            }
            
        });
        VBox vbox = new VBox(20, fieldBox1,fieldBox2, buttonBox);
        return new Scene(vbox, 600, 600);
    }

    private void handleSignup(Stage primaryStage,String username, String password) {

        DataService dataService;
        try {
            
            dataService = new DataService();
            Map<String, Object> data = new HashMap<>();
            data.put("password", password);

            data.put("username", username);
            dataService.addData("users", username,data);
            System.out.println("User registered successfully");
            loginController.showLoginScene();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
