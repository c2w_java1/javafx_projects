package com.example.controller;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import java.util.concurrent.ExecutionException;

import com.example.dashbord.UserPage;
import com.example.firebaseCoding.DataService;

public class LoginController {
    

    private Stage primaryStage;
    private Scene loginScene;
    private Scene userScene;
    private DataService dataService;
    private Scene adminScene;
    public static String key;

    public LoginController(Stage primaryStage) {
        
        this.primaryStage = primaryStage;
        dataService = new DataService(); 
        //Initialize DataService instance
        initScenes(); // Initialize scenes
    }

    public void setPrimaryStage(Scene scene) {
        if (primaryStage != null) {
            primaryStage.setScene(scene);
        } else {
            System.err.println("Primary stage is not set. Cannot set scene.");
        }
    }

    private void initScenes() {
        initLoginScene();
    }

    private void initLoginScene() {
        
        Label userLabel = new Label("Username");

        TextField userTextField = new TextField();
        Label passLabel = new Label("Password");
        PasswordField passField = new PasswordField();

        Button loginButton = new Button("Login");
        Button signupButton = new Button("Signup");

        loginButton.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                
                handleLogin(userTextField.getText(),passField.getText());
                userTextField.clear();
                passField.clear();
            }
            
        });

        signupButton.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                
                showSignupScene();
                userTextField.clear();
                passField.clear();
            }
            
        });

        userLabel.setStyle("-fx-text-fill:white;");
        passLabel.setStyle("-fx-text-fill:white;");

        VBox fieldBox1 = new VBox(10, userLabel,userTextField);
        fieldBox1.setMaxSize(300, 30);
        VBox fieldBox2 = new VBox(10, passLabel,passField);

        fieldBox2.setMaxSize(300, 30);
        HBox buttonBox = new HBox(50, loginButton, signupButton);

        buttonBox.setMaxSize(350, 30);
        buttonBox.setAlignment(Pos.CENTER);

        userTextField.setStyle("-fx-set-pref-width:350");
        passField.setStyle("-fx-set-pref-width:350");

        VBox vbox = new VBox(20, fieldBox1, fieldBox2, buttonBox);
        vbox.setStyle("-fx-background-image:url('https://img.freepik.com/free-vector/flat-design-collage-background_23-2149545328.jpg?t=st=1719821174~exp=1719824774~hmac=c7d6a6f5cea59375167ec31a61ac73c4d36e21f87d9a7ad9cd2a6fd695b01066&w=996')");

        vbox.setAlignment(Pos.CENTER);
        loginScene = new Scene(vbox, 600, 600);
    }
    
    private void initUserScene() {

        UserPage userPage = new UserPage(dataService);
        userScene = new Scene(userPage.createUserScene(this::handleLogout),600, 600); 
    }

    public Scene getLoginScene() {
        return loginScene;
    }

    public void showLoginScene() {
        primaryStage.setScene(loginScene);
        primaryStage.setTitle("Login");
        
        primaryStage.show();
    }

    private void handleLogin(String username, String password) {

        try {
            if(dataService.authenticateUser(username,password)){ 
            
                key = username;
                initUserScene();
                primaryStage.setScene(userScene);
                primaryStage.setTitle("UserDashboard");
            } else{

                System.out.println("Invalid credentials");
                key = null;
            }
        } catch (ExecutionException | InterruptedException ex) {
            
            ex.printStackTrace();
        }
    }

    private void showSignupScene() {

        SignupController signupController = new SignupController(this);
        Scene signupScene = signupController.createSignupScene(primaryStage);

        primaryStage.setScene(signupScene);
        primaryStage.setTitle("Signup");
        primaryStage.show();
    }

    private void handleLogout() {
        primaryStage.setScene(loginScene);
        primaryStage.setTitle("Login");
    }

    public void returnToAdminView() {
        
        primaryStage.setScene(adminScene);
        primaryStage.setTitle("Admin Dashboard");
    }

    
}
