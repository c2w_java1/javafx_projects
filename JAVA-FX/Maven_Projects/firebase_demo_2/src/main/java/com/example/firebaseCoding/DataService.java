package com.example.firebaseCoding;


import com.google.api.core.ApiFuture;
import com.google.auth.oauth2.GoogleCredentials;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.google.firebase.cloud.FirestoreClient;
import com.google.cloud.firestore.*;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;

public class DataService {

    private static Firestore db;
   
    static {
        try {
            initializeFirebase();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @SuppressWarnings("deprecation")
    private static void initializeFirebase() throws IOException {
        FileInputStream serviceAccount = new FileInputStream("D:\\javafx_projects\\JAVA-FX\\Maven_Projects\\firebase_demo_2\\src\\main\\resources\\firebase-2nd.json");
        
        FirebaseOptions options = new FirebaseOptions.Builder()
          .setCredentials(GoogleCredentials.fromStream(serviceAccount))
          .setDatabaseUrl("https://fir-2nd-57b17-default-rtdb.asia-southeast1.firebasedatabase.app")
          .build();
        
        FirebaseApp.initializeApp(options);
        
        db = FirestoreClient.getFirestore();
    }

    // Method to add data to a specified collection and document
    public void addData(String collection, String document, Map<String, Object> data) throws ExecutionException,InterruptedException {

            DocumentReference docRef = db.collection(collection).document(document);
            ApiFuture<WriteResult> result = docRef.set(data);
            // Block until the write operation is complete
            result.get();
    }
// Method to retrieve data from a specified collection and document

    public DocumentSnapshot getData(String collection, String document) throws ExecutionException, InterruptedException {

        try { 
            DocumentReference docRef = db.collection(collection).document(document);
            ApiFuture<DocumentSnapshot> future = docRef.get();
            return future.get();
        } catch (Exception e) {

            e.printStackTrace();
            // Re-throw the exception or handle it based on your application's needs
            throw e;
        }
}
// Method to authenticate a user by comparing the provided password with the stored password

public boolean authenticateUser(String username, String password) throws ExecutionException, InterruptedException {
        
    // Retrieve the document for the user with the given username
    DocumentSnapshot document = db.collection("users").document(username).get().get();
    
    if (document.exists()) {
        // Get the stored password from the document
        String storedPassword = document.getString("password");
        // Compare the provided password with the stored password
        return password.equals(storedPassword);
    }
    
    return false;
    }

    public boolean isAdmin(String username) throws ExecutionException,InterruptedException {
       
        DocumentSnapshot document = db.collection("users").document(username).get().get();
        if (document.exists()) {
            String role = document.getString("role");
            System.out.println(role);
            return "admin".equals(role);
    }

         return false;
    }

    public void deleteProject(String string, String string2) {
        
        System.out.println(leaderName);
        ApiFuture<WriteResult> writeResult = db.collection(collectionName).document(leaderName).delete();
        writeResult.get();
        
    }
    public List<Map<String, Object>> getDataInDescendingOrder(String string, String string2) {
        List<Map<String, Object>> documentsList = new ArrayList<>();

        CollectionReference collection = db.collection(collectionName);
        Query query = collection.orderBy(orderByField, Query.Direction.DESCENDING);

        ApiFuture<QuerySnapshot> querySnapshot = query.get();
        for (DocumentSnapshot document : querySnapshot.get().getDocuments()) {
            documentsList.add(document.getData());
        }

    return documentsList;
    }
   
    public void updateData(String string, String string2, Map<String, Object> updatedData) throws ExecutionException, InterruptedException {
            CollectionReference collection = db.collection(collectionName);
            DocumentReference docRef = collection.document(documentId);
            ApiFuture<WriteResult> future = docRef.set(updatedData, SetOptions.merge());
            future.get();
        }
}
