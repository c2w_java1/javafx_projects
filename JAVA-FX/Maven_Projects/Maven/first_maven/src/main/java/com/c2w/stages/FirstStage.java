package main.java.com.c2w.stages;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

public class FirstStage extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception {
        
    

        primaryStage.setTitle("MyFirstProjectStage");
        primaryStage.getIcons().add(new Image("images/c.png"));
        
        primaryStage.setHeight(1000);
        primaryStage.setWidth(1080);
        primaryStage.setResizable(false);
        
        primaryStage.show();

        
    }
    
}
