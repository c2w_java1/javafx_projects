package com.pojo_demo;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        System.out.println("Players Data...!\n-------------------------------------------");

        PlayersData pd = new PlayersData();

        /* 
        //Hardcoded Data
        pd.setPlayerName("Virat Kohli");
        pd.setPlayerTeam("India");
        pd.setJerNo(18);
        pd.setAge(35);

        //To print/get data
        System.out.println(pd.getPlayerName());
        System.out.println(pd.getPlayerTeam());
        System.out.println(pd.getJerNo());
        System.out.println(pd.getAge());
        */

        Scanner sc = new Scanner(System.in);

        System.out.println("Enter player name : ");
        String name = sc.nextLine();

        System.out.println("Enter player Team : ");
        String team = sc.nextLine();

        System.out.println("Enter player jerNo : ");
        int jerNo = sc.nextInt();

        System.out.println("Enter age");
        int age = sc.nextInt();

        pd.setPlayerName(name);
        pd.setPlayerTeam(team);
        pd.setJerNo(jerNo);
        pd.setAge(age);
        
        System.out.println("--------------------------------------------------");
        System.out.println(pd.getPlayerName());
        System.out.println(pd.getPlayerTeam());
        System.out.println(pd.getJerNo());
        System.out.println(pd.getAge());

        System.out.println("--------------------------------------------------");
        

    }
}