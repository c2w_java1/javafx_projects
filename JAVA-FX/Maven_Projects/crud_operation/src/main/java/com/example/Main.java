package com.example;

import java.io.IOException;
import java.util.concurrent.ExecutionException;

import com.example.initialize.FirebaseInit;

public class Main {
    public static void main(String[] args) throws IOException,
        InterruptedException, ExecutionException {
        FirebaseInit obj = new FirebaseInit();
        FirebaseInit.initializeFirebase();
        obj.createRec();
        //obj.readRec();
        obj.updateRec();
    }
}