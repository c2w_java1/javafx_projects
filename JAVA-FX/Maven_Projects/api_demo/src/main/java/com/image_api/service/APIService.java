package com.image_api.service;

import java.io.IOException;

import org.json.JSONObject;

import javafx.application.Application;

import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

public class APIService extends Application {

    //string to store image url
    static String imgURL = "https://api.unsplash.com/photos/random?client_id=xXDyucMrjFsFY0jBt6B6Qd9iOJDl4lB2G1gtMoHbvEM";

    public static void imageData()throws IOException{

        StringBuffer responce = new DataUrls().getResponseData();
        if (responce != null) {
            
            JSONObject obj = new JSONObject(responce.toString());
            JSONObject urlobj = obj.getJSONObject("urls");

            imgURL = urlobj.getString("small");
        } else {

            System.out.println("Responce is empty");
        }
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
       
        imageData();

        Image image = new Image(imgURL);

        ImageView imageView = new ImageView(image);

        Pane iPane = new Pane();
        iPane.getChildren().add(imageView);
        
        Scene sc = new Scene(iPane,image.getWidth(),image.getHeight());
        primaryStage.setScene(sc);
        primaryStage.setTitle("API_Images_Example");
        primaryStage.show();

    }
    
}
