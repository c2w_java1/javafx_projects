package com.image_api.service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import javax.management.RuntimeErrorException;

public class DataUrls {
    
    public StringBuffer getResponseData()throws IOException{

        String url = "https://api.unsplash.com/photos/random?client_id=xXDyucMrjFsFY0jBt6B6Qd9iOJDl4lB2G1gtMoHbvEM";
        HttpURLConnection httpClient = (HttpURLConnection) new URL(url).openConnection();
        httpClient.setRequestMethod("GET");
        StringBuffer response = new StringBuffer();

        int responceCode = httpClient.getResponseCode();

        if (responceCode == HttpURLConnection.HTTP_OK) {
            BufferedReader in = new BufferedReader(new InputStreamReader(httpClient.getInputStream()));
            String inputLine;

            while ((inputLine=in.readLine())!= null) {
                response.append(inputLine);

            }
            in.close();
            return response;
        } else {
            throw new RuntimeException("Request Fail with responce Code "+ responceCode);
        }
    }
}
