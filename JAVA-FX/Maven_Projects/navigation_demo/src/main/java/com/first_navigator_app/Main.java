package com.first_navigator_app;

import com.first_navigator_app.controller.FormNavigationApp;

import javafx.application.Application;

public class Main {
    public static void main(String[] args) {
        System.out.println("Hello world!");
        Application.launch(FormNavigationApp.class,args);
    }
}