package com.first_navigator_app.dataAccess;

import com.first_navigator_app.controller.FormNavigationApp;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;

public class FormPage1 {

    private FormNavigationApp app;
    private GridPane view;
    private TextField field1;

    public FormPage1 (FormNavigationApp app){

        this.app=app;
        initialize();
    }

    private void initialize(){

        view = new GridPane();
        view.setPadding(new Insets(10));
        view.setHgap(10);
        view.setVgap(10);

        Label lb1 = new Label("Field 1 :");
        field1 = new TextField();

        Button nb = new Button("Next");
        nb.setOnAction(new EventHandler<ActionEvent>() {

         @Override
         public void handle(ActionEvent event) {
            app.navigatorPage2();
         }
         
        });

        Button cb = new Button("Clear");
        cb.setOnAction(new EventHandler<ActionEvent>() {

         @Override
         public void handle(ActionEvent event) {
            
            setField1Value(null);
         }
         
        });


        view.add(lb1, 0, 0);
        view.add(field1, 1, 0);
        view.add(nb, 2, 1);
        view.add(cb, 3, 1);   
     }

     public GridPane getView(){
        return view;
     }

     public String getField1dValue(){
        return field1.getText();
     }

     public void setField1Value(String value){
        field1.setText(value);
     }
    
}
