package com.first_navigator_app.dataAccess;

import com.first_navigator_app.controller.FormNavigationApp;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;

public class FormPage2 {

    private FormNavigationApp app;
    private GridPane view;
    private TextField field2;

    public FormPage2(FormNavigationApp app) {
   
        this.app = app;
        initialize();
    }

    private void initialize() {
        
        view = new GridPane();
        view.setPadding(new Insets(10));
        view.setHgap(10);
        view.setVgap(10);

        Label lb2 = new Label("Field 2 :");
        field2 = new TextField();

        Button nb = new Button("Next");
        nb.setOnAction(new EventHandler<ActionEvent>() {

         @Override
         public void handle(ActionEvent event) {
            
            app.navigatorPage3();
         }
         
        });

        Button bb = new Button("Back");
        bb.setOnAction(new EventHandler<ActionEvent>() {

         @Override
         public void handle(ActionEvent event) {
            
            app.navigatorPage1();
         }
         
        });

        Button cb = new Button("Clear");
        cb.setOnAction(new EventHandler<ActionEvent>() {

         @Override
         public void handle(ActionEvent event) {
            
            setField2Value(null);
         }
         
        });

        view.add(lb2, 0, 0);
        view.add(field2, 1, 0);
        view.add(nb, 2, 1);
        view.add(bb, 1, 1);
        view.add(cb, 3, 1); 
    }

    public GridPane getView(){
        return view;
    }
    public String getField2dValue() {
        
        return field2.getText();
    }


    public void setField2Value(String value) {
       field2.setText(value);
    }

}
