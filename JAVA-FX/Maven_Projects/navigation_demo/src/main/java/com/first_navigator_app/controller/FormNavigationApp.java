package com.first_navigator_app.controller;

import com.first_navigator_app.dataAccess.FormPage1;
import com.first_navigator_app.dataAccess.FormPage2;
import com.first_navigator_app.dataAccess.FormPage3;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class FormNavigationApp extends Application{

    private Stage primaryStage;
    private Scene page1Scene,page2Scene,page3Scene;

    private FormPage1 page1;
    private FormPage2 page2;
    private FormPage3 page3;
    
    @Override
    public void start(Stage primaryStage) throws Exception {

        this.primaryStage = primaryStage;
        
        page1 = new FormPage1(this);
        page2 = new FormPage2(this);
        page3 = new FormPage3(this);

        page1Scene = new Scene(page1.getView(),600,400);
        page2Scene = new Scene(page2.getView(),600,400);
        page3Scene = new Scene(page3.getView(),600,400);
        
        primaryStage.setScene(page1Scene);
        primaryStage.setTitle("Navigator_App");
        primaryStage.show();
    }

    public void navigatorPage1(){
        
        page2.setField2Value(page2.getField2dValue());
        page1.setField1Value(page1.getField1dValue());
        
        primaryStage.setScene(page1Scene);
    }
    
    public void navigatorPage2(){
        
        page1.setField1Value(page1.getField1dValue());
        page3.setField3Value(page3.getField3dValue());
        
        
        primaryStage.setScene(page2Scene);
    }
    
    public void navigatorPage3(){
        
        page2.setField2Value(page2.getField2dValue());
        
        primaryStage.setScene(page3Scene);
    }
    

}
