package javaFiles.home;

import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class HomeWindow extends Application{

    Text tital =  null;
    Text java = null;
    Text cpp = null;
    Text python = null;

    @Override
    public void start(Stage prStage){

        
        Text gm = new Text(20,40,"Good Morning");
        gm.setFont(new Font(30));
        gm.setFill(Color.RED);

        Text hm = new Text(600,40,"Have a Nice Day.....!");
        hm.setFont(new Font(30));
        hm.setFill(Color.RED);

        tital = new Text(100,150, "Programming languages.");
        tital.setFont(new Font(70));
        tital.setFill(Color.WHITE);

        cpp = new Text(10,200, "cpp");
        cpp.setFont(new Font(30));
        cpp.setFill(Color.ORANGE);

        python = new Text(10,300, "python");
        python.setFont(new Font(30));
        python.setFill(Color.WHITE);

        java = new Text(10,400, "java");
        java.setFont(new Font(30));
        java.setFill(Color.GREEN);


        Text lib = new Text(20,500,"Lib");
        lib.setFill(Color.RED);
        lib.setFont(new Font(30));

        Text src = new Text(20,600,"src");
        src.setFill(Color.RED);
        src.setFont(new Font(30));

        Text bin = new Text(20,700,"bin");
        bin.setFill(Color.RED);
        bin.setFont(new Font(30));

        VBox vb1 = new VBox(50,bin,lib ,src);
        vb1.setLayoutX(600);
        vb1.setLayoutY(500);

        VBox vb2 = new VBox(50,cpp,java,python);
        vb2.setLayoutX(500);
        vb2.setLayoutY(500);

        HBox hb = new HBox(50,vb1,vb2);
        hb.setLayoutX(400);
        hb.setLayoutY(400);

        Group gr = new Group(hm,gm,tital,hb);

        Scene sc = new Scene(gr);
        sc.setFill(Color.BLACK);
    
        prStage.setScene(sc);

        prStage.setTitle("MyFirstProjectStage");
        prStage.getIcons().add(new Image("assets/image/shambhuraje.jpg"));
        prStage.setHeight(1000);
        prStage.setWidth(1080);
        prStage.setResizable(false);
        
        prStage.show();
    }
}
