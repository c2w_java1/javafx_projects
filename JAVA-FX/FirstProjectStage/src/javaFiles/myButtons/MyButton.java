package javaFiles.myButtons;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Border;
import javafx.scene.layout.BorderStroke;
import javafx.scene.layout.BorderStrokeStyle;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class MyButton extends Application {

    @Override
    public void start(Stage primaryStage) {
       
        Text tital = new Text("Button Page");
        tital.setFont(new Font(60));
        tital.setFill(Color.ORANGE);

        Text name = new Text("User Name");
        name.setFont(new Font(20));
        VBox vbName = new VBox(name);

        TextField tf = new TextField();
        tf.setFont(new Font(20));

        Text pass = new Text("Password");
        pass.setFont(new Font(20));
        VBox vbPass = new VBox(pass);
        
        PasswordField pf = new PasswordField();
        pf.setFont(new Font(20));
        HBox hb1 = new HBox(pf);
        


        Button show = new Button("Show");
        show.setFont(new Font(20));

        
        show.setAlignment(Pos.CENTER);
        Label lb = new Label();
        lb.setFont(new Font(40));
        Label lb2 = new Label();
        lb2.setFont(new Font(40));
        show.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
               
               lb.setText(tf.getText());
               lb2.setText(pf.getText());
               System.out.println(tf.getText());
               System.out.println(pf.getText());
               
            }
            
        });

        VBox vb = new VBox(20,tital,vbName,tf,vbPass,hb1,show,lb,lb2);
        
        vb.setLayoutX(600);
        vb.setLayoutY(200);
        vb.setStyle("-fx-background-color:Blue");
        vb.setPrefSize(600, 600);
        vb.setAlignment(Pos.CENTER);
       // vb.setBorder(new BorderStroke(Color.BLUEVIOLET, BorderStrokeStyle.SOLID, CornerRadii.EMPTY, null));
 /* 
        Image ig = new Image("assets/image/java.png");
        ImageView iv = new ImageView(ig);
        iv.setFitHeight(1000);
        iv.setFitWidth(1800);
    

*/
        Group gr = new Group(vb);
        Scene sc = new Scene(gr);
        sc.setFill(Color.BROWN);

        primaryStage.setScene(sc);
        primaryStage.setTitle("MyButtonStage");
        
        primaryStage.setHeight(1000);
        primaryStage.setWidth(1800);
        primaryStage.setResizable(false);
        primaryStage.show();
    }
}
